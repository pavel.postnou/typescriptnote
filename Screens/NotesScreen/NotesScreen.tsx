import React, { useState, useEffect } from "react";
import { View, StyleSheet, ImageBackground } from "react-native";
import { Icon } from "react-native-elements";
import { doc, updateDoc, onSnapshot } from "firebase/firestore";

import ListItemComponent from "../../src/shared/components/ListItemComponent/ListItemComponent";
import { ModalComponent } from "../../src/shared/components/ModalComponent/ModalComponent";
import { img } from "../../constants/backgroundImage";
import { width, height, statusHeight } from "../../constants/sizeConstant";
import { db } from "../../firebase";
import { ScrollView } from "react-native-gesture-handler";
import { SettingsComponent } from "../../src/shared/components/SettingsComponent";

const NotesScreen = () => {
  const [active, setActive] = useState<boolean>(false);
  const [notes, setNotes] = useState<Array<{ title: string; text: string }>>(
    []
  );
  const [note, setNote] = useState<{ title: string; text: string }>();
  const [delActive, setDelActive] = useState<boolean>(false);
  const [settings, setSettings] = useState<boolean>(false);

  useEffect(() => {
    onSnapshot(doc(db, "notes", "mynotes"), (doc) => {
      if (doc.data()) {
        setNotes(doc.data()?.notes);
      }
    });
  }, []);

  useEffect(() => {
    (async function updateNote() {
      if (note) {
        let arr: Array<{}> = [];
        notes ? (arr = notes) : null;
        console.log(note);
        arr.push(note);
        await updateDoc(doc(db, "notes", "mynotes"), {
          notes: arr,
        });
        setNote(undefined);
      }
    })();
  }, [note]);

  return (
    <View style={styles.containerAll}>
      <ImageBackground style={styles.backgroundImage} source={img}>
        <View style={styles.container}>
          <ScrollView>
            {notes?.map((item, index) => (
              <ListItemComponent
                key={index}
                title={item.title}
                text={item.text}
                delActive={delActive}
              />
            ))}
          </ScrollView>
          <View style={styles.addIcon}>
            <Icon
              name="add-circle-outline"
              type="ionicons"
              size={50}
              color="blue"
              onPress={() => setActive(!active)}
            />
          </View>
          <View style={styles.delIcon}>
            <Icon
              name="trash-o"
              type="font-awesome"
              size={50}
              color="red"
              onPress={() => setDelActive(!delActive)}
            />
          </View>
          <View style={styles.settingsIcon}>
            <Icon
              name="cog"
              type="font-awesome"
              size={50}
              color="grey"
              onPress={() => setSettings(!settings)}
            />
          </View>
          <ModalComponent
            active={active}
            setActive={setActive}
            setNote={setNote}
          />
          <SettingsComponent settings={settings} setSettings={setSettings} />
        </View>
      </ImageBackground>
    </View>
  );
};

export { NotesScreen };

const styles = StyleSheet.create({
  containerAll: {
    height: height,
    width: width,
    marginTop: statusHeight,
  },
  container: {
    height: "100%",
    width: "100%",
    alignItems: "center",
  },
  addIcon: {
    position: "absolute",
    bottom: 40,
    alignSelf: "center",
  },
  delIcon: {
    position: "absolute",
    bottom: 40,
    right: 30,
  },
  settingsIcon: {
    position: "absolute",
    bottom: 40,
    left: 30,
  },
  backgroundImage: {
    width: "100%",
    height: "100%",
  },
});
