import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  Modal,
  View,
  TouchableHighlight,
} from "react-native";
import { Icon, CheckBox } from "react-native-elements";

import { width, height } from "../../../../constants/sizeConstant";

type Props = {
  settings: boolean;
  setSettings: React.Dispatch<React.SetStateAction<boolean>>;
};

const SettingsComponent = (props: Props) => {
  const { settings, setSettings } = props;
  const [check1, setCheck1] = useState(false);
  return (
    <Modal animationType="fade" transparent={true} visible={settings}>
      <View style={styles.container}>
        <View style={styles.centeredView}>
          <Text style={styles.title}>TITLE SETTINGS</Text>
          <CheckBox
            center
            title="Italic Font"
            checked={check1}
            onPress={() => setCheck1(!check1)}
          />
          <TouchableHighlight
            style={styles.touchable}
            onPress={() => setSettings(!settings)}
          >
            <View style={styles.button}>
              <Text style={styles.textButton}>SAVE</Text>
            </View>
          </TouchableHighlight>
        </View>
      </View>
    </Modal>
  );
};

export { SettingsComponent };

const styles = StyleSheet.create({
  container: {
    height: height - 100,
    width: width - 100,
    alignSelf: "center",
    justifyContent: "center",
  },
  centeredView: {
    alignSelf: "center",
    backgroundColor: "#e6f9ff",
    width: width - 100,
    borderRadius: 10,
  },
  title: {
    alignSelf: "center",
    color: "blue",
    fontWeight: "bold",
    margin: 10,
  },
  text: {
    alignSelf: "center",
    color: "red",
    fontWeight: "bold",
    margin: 10,
  },
  inputTitle: {
    padding: 5,
    backgroundColor: "white",
    margin: 10,
    borderRadius: 10,
  },
  inputText: {
    padding: 5,
    backgroundColor: "white",
    margin: 10,
    borderRadius: 10,
    height: 150,
  },
  close: {
    position: "absolute",
    top: 10,
    right: 10,
  },
  touchable: {
    alignSelf: "center",
    marginBottom: 10,
  },
  button: {
    width: 100,
    height: 30,
    backgroundColor: "blue",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  textButton: {
    color: "white",
    fontWeight: "bold",
  },
});
