import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  Modal,
  View,
  TextInput,
  TouchableHighlight,
} from "react-native";
import { Icon } from "react-native-elements";

import { width, height } from "../../../../constants/sizeConstant";

type Props = {
  active: boolean;
  setActive: React.Dispatch<React.SetStateAction<boolean>>;
  setNote: React.Dispatch<
    React.SetStateAction<{ title: string; text: string } | undefined>
  >;
};

const ModalComponent = (props: Props) => {
  const { active, setActive, setNote } = props;
  const [title, setTitle] = useState("");
  const [text, setText] = useState("");

  return (
    <Modal animationType="fade" transparent={true} visible={active}>
      <View style={styles.container}>
        <View style={styles.centeredView}>
          <Text style={styles.title}>TITLE</Text>
          <TextInput
            style={styles.inputTitle}
            placeholder="title"
            value={title}
            onChangeText={(text) => setTitle(text)}
          />
          <Text style={styles.text}>TEXT</Text>
          <TextInput
            multiline
            style={styles.inputText}
            placeholder="text"
            value={text}
            onChangeText={(text) => setText(text)}
          />
          <View style={styles.close}>
            <Icon
              name="close"
              type="ionicons"
              size={30}
              color="black"
              onPress={() => (setActive(!active), setText(""), setTitle(""))}
            />
          </View>
          <TouchableHighlight
            style={styles.touchable}
            onPress={() =>
              text && title
                ? (setNote({ title: title, text: text }),
                  setActive(!active),
                  setText(""),
                  setTitle(""))
                : alert("Введите текст")
            }
          >
            <View style={styles.button}>
              <Text style={styles.textButton}>ADD</Text>
            </View>
          </TouchableHighlight>
        </View>
      </View>
    </Modal>
  );
};

export { ModalComponent };

const styles = StyleSheet.create({
  container: {
    height: height - 100,
    width: width - 100,
    alignSelf: "center",
    justifyContent: "center",
  },
  centeredView: {
    alignSelf: "center",
    backgroundColor: "#e6f9ff",
    width: width - 100,
    borderRadius: 10,
  },
  title: {
    alignSelf: "center",
    color: "blue",
    fontWeight: "bold",
    margin: 10,
  },
  text: {
    alignSelf: "center",
    color: "red",
    fontWeight: "bold",
    margin: 10,
  },
  inputTitle: {
    padding: 5,
    backgroundColor: "white",
    margin: 10,
    borderRadius: 10,
  },
  inputText: {
    padding: 5,
    backgroundColor: "white",
    margin: 10,
    borderRadius: 10,
    height: 150,
  },
  close: {
    position: "absolute",
    top: 10,
    right: 10,
  },
  touchable: {
    alignSelf: "center",
    marginBottom: 10,
  },
  button: {
    width: 100,
    height: 30,
    backgroundColor: "blue",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
  },
  textButton: {
    color: "white",
    fontWeight: "bold",
  },
});
