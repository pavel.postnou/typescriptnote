import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { ListItem, Icon } from "react-native-elements";
import { arrayRemove, doc, updateDoc } from "firebase/firestore";

import { width } from "../../../../constants/sizeConstant";
import { db } from "../../../../firebase";

type Props = { title: string; text: string; delActive: boolean };

const ListItemComponent = (props: Props) => {
  const [expanded, setExpanded] = React.useState(false);
  const { title, text, delActive } = props;

  const updateNote = async (title: string, text: string) => {
    await updateDoc(doc(db, "notes", "mynotes"), {
      notes: arrayRemove({ title: title, text: text }),
    });
  };

  return (
    <View style={styles.container}>
      <ListItem.Accordion
        style={styles.accordion}
        containerStyle={styles.accordion}
        content={
          <>
            <Icon
              name="sticky-note-o"
              type="font-awesome"
              size={20}
              color="red"
              style={styles.listIcon}
            />
            <ListItem.Content>
              <ListItem.Title style={styles.title}>{title}</ListItem.Title>
            </ListItem.Content>
          </>
        }
        isExpanded={expanded}
        onPress={() => {
          setExpanded(!expanded);
        }}
      >
        <View style={styles.noteContainer}>
          <Text style={styles.noteText}>{text}</Text>
        </View>
      </ListItem.Accordion>
      <View style={styles.delIcon}>
        {delActive ? (
          <Icon
            name="trash-o"
            type="font-awesome"
            size={20}
            color={delActive ? "red" : "transparent"}
            onPress={() => updateNote(title, text)}
          />
        ) : null}
      </View>
    </View>
  );
};

export default React.memo(ListItemComponent);

const styles = StyleSheet.create({
  container: {
    width: width,
    alignSelf:"center"
  },
  noteContainer: {
    width: width - 100,
    padding: 10,
    borderRadius: 10,
    backgroundColor: "rgb(173, 217, 255)",
    marginBottom: 10,
    marginLeft: 15,
  },
  accordion: {
    width: width - 70,
    borderRadius: 20,
    overflow: "hidden",
    backgroundColor: "rgb(217, 217, 217)",
    marginBottom: 10,
    alignSelf:"center",
    marginLeft: 10,
  },
  listIcon: {
    marginHorizontal: 10,
  },
  title: {
    fontWeight: "bold",
    textDecorationLine:"underline",
    textDecorationStyle:"solid",
    color: "#fffa96",
    fontSize: 20,
  },
  noteText: {
    color: "white",
    fontSize: 15,
    fontStyle: "italic",
  },
  addIcon: {
    position: "absolute",
    bottom: 40,
    right: 30,
  },
  delIcon: {
    position: "absolute",
    right: 10,
    top: 20,
  },
  backgroundImage: {
    width: "100%",
    height: "100%",
  },
});
