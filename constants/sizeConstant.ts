import { Dimensions, StatusBar } from "react-native";

const { width, height } = Dimensions.get("window");
const statusHeight = StatusBar.currentHeight

export { width, height, statusHeight };
