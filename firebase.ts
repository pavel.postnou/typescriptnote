// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDdjcNMZa_YQe5Z0VJXcy4yhkQoW2O21LE",
  authDomain: "notes-b46f5.firebaseapp.com",
  projectId: "notes-b46f5",
  storageBucket: "notes-b46f5.appspot.com",
  messagingSenderId: "652275453881",
  appId: "1:652275453881:web:94ccea85ef432802d73068",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore();

export { db };
